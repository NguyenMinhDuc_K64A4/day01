CREATE DATABASE QLSV;

CREATE TABLE IF NOT EXISTS QLSV.DMKHOA
(
    MaKH varchar(6),
    TenKhoa varchar(30),
    Primary Key(MaKH)
);

CREATE TABLE IF NOT EXISTS QLSV.SINHVIEN
(
    	MaSV varchar(6),
    	HoSV varchar(30),
	TenSV varchar(15),
	GioiTinh char(1),
	NgaySinh DateTime,
	NoiSinh varchar(50),
	DiaChi varchar(50),
	MaKH varchar(6),
	HocBong int,
	Primary Key(MaSV)
);


SELECT * FROM SINHVIEN, DMKHOA WHERE SINHVIEN.MaKH = DMKHOA.TenKhoa and DMKHOA.TenKhoa = 'Công nghệ thông tin';
